package com.example.object;

import java.net.MalformedURLException;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.TableQueryCallback;
import com.ravolo.lib.android.nav.NavDrawerActivity;

public class RecordSessionActivity extends NavDrawerActivity {
	MobileServiceTable<InterviewData> mToDoTable;
	ProjectManager  mProjectHandler;
	ArrayAdapter<InterviewData> arrayAdapter ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record_session);
		mProjectHandler = new ProjectManager(this);
		arrayAdapter = new ArrayAdapter<InterviewData>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
		MobileServiceClient mClient;
		try {
			mClient = new MobileServiceClient(
					"https://object.azure-mobile.net/",
					"cvnQzPqZEvwdjPOdpCQYlXoSNbSMMz99", RecordSessionActivity.this);
			mToDoTable = mClient.getTable("recordingbucket",InterviewData.class);
			
			final ListView sessionListView = (ListView) findViewById(R.id.sessionListView);
			sessionListView.setAdapter(arrayAdapter);
			//sessionListView.setAdapter();
			mToDoTable.where().field("project").eq(mProjectHandler.getCurrentProject()).execute(new TableQueryCallback<InterviewData>() {

				@Override
				public void onCompleted(List<InterviewData> arg0, int arg1,
						Exception arg2, ServiceFilterResponse arg3) {
					for(InterviewData data:arg0){
						arrayAdapter.add(data);
					}
					arrayAdapter.notifyDataSetChanged();
					// TODO Auto-generated method stub
					
				}
				
			});
			sessionListView.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					
					InterviewData data = arrayAdapter.getItem(position);
					Intent intent = new Intent(RecordSessionActivity.this,ASessionActivity.class);
					intent.putExtra("INTERVIEW_DATA", data);
					startActivity(intent);
				}
				
			});
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.record_session, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected String[] getNavDrawerListTitles() {
		// TODO Auto-generated method stub
		return AppConstants.NAV_DRAWER_TITLE;
	}

	@Override
	protected void selectedDrawerItem(int position) {
		switch (position) {
		case 0:
			Intent intent3 = new Intent(this, ToDoActivity.class);
			startActivity(intent3);
			break;
		case 1:
			Intent intent = new Intent(this,RecordInterviewActivity.class);
			startActivity(intent);
			break;
		case 2:
			//sessin
			break;
		case 3:
			new ProjectManager(this).switchProject();
			Intent intent2 = new Intent(this,ProjectSelectionActivity.class);
			startActivity(intent2);
			break;
		default:
			break;
		}

	}


}
