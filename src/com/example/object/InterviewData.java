package com.example.object;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InterviewData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4033041633705654935L;
	private String audioFileNameInBucket;
	private String handlerName = "";
	private String interviewName;
	private String contactNumber;
	private String occupation;
	private String age;
	private String notes;
	private String dateOnly = "";
	/**
	 * Item Id
	 */
	@com.google.gson.annotations.SerializedName("id")
	private String mId;
	private String url;
	
	
	public String getId() {
		return mId;
	}

	public void setId(String mId) {
		this.mId = mId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setInterviewName(String name){
		this.interviewName = name;
		audioFileNameInBucket = name+"_"+new Date().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		dateOnly = dateFormat.format(date);
	}

	public String getAudioFileNameInBucket() {
		return audioFileNameInBucket;
	}

	public void setAudioFileNameInBucket(String audioFileNameInBucket) {
		this.audioFileNameInBucket = audioFileNameInBucket;
	}

	public String getHandlerName() {
		return handlerName;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getInterviewName() {
		return interviewName;
	}
	
	public String project;


	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}
	
	@Override
	public String toString(){
		return interviewName + "\n"+dateOnly;
	}
	
	
	
}
