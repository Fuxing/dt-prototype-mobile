package com.example.object;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.ServiceFilterResponseCallback;
import com.microsoft.windowsazure.mobileservices.TableOperationCallback;
import com.microsoft.windowsazure.mobileservices.TableQueryCallback;
import com.ravolo.lib.android.nav.NavDrawerActivity;

public class ASessionActivity extends NavDrawerActivity {

	/**
	 * Mobile Service Client reference
	 */
	private MobileServiceClient mClient;

	/**
	 * Mobile Service Table used to access data
	 */
	private MobileServiceTable<ToDoItem> mToDoTable;

	/**
	 * Adapter to sync the items list with the view
	 */
	private ToDoItemAdapter mAdapter;

	/**
	 * EditText containing the "New ToDo" text
	 */
	private EditText mTextNewToDo;
	/**
	 * Progress spinner to use for table operations
	 */
	private ProgressBar mProgressBar;

	ProjectManager  mProjectHandler = new ProjectManager(this);
	/**
	 * Initializes the activity
	 */
	InterviewData data;
	 private static final int CAMERA_REQUEST = 1888; 
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private Uri fileUri;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri(int type){
	      return Uri.fromFile(getOutputMediaFile(type));
	}
	private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
	private AmazonS3Client s3Client = new AmazonS3Client
			(
			new BasicAWSCredentials(AppConstants.ACCESS_KEY,
					AppConstants.SECRET_KEY));
	String fileLocation;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
	        if (resultCode == RESULT_OK) {
	            // Image captured and saved to fileUri specified in the Intent
	            fileLocation = data.getData().toString();
	            uploadData();
	        } else if (resultCode == RESULT_CANCELED) {
	            // User cancelled the image capture
	        } else {
	            // Image capture failed, advise user
	        }
	    }

	    if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {

	    }
	    Bitmap photo = (Bitmap) data.getExtras().get("data"); 


        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        Uri tempUri = getImageUri(getApplicationContext(), photo);

        // CALL THIS METHOD TO GET THE ACTUAL PATH
        fileLocation = getRealPathFromURI(tempUri);
        uploadData();
	}
	
	public Uri getImageUri(Context inContext, Bitmap inImage) {
	    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
	    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
	    String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
	    return Uri.parse(path);
	}

	public String getRealPathFromURI(Uri uri) {
	    Cursor cursor = getContentResolver().query(uri, null, null, null, null); 
	    cursor.moveToFirst(); 
	    int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	    return cursor.getString(idx); 
	}
	private void uploadData() {
		new UploadRecordingTask().execute();
	}
	public class PhotoData{
		String id;
		
		String fileURL;
		String project;
		String interviewName;
	}
	private class UploadRecordingTask extends AsyncTask<String,String,String>{

		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
		dialog = new ProgressDialog(ASessionActivity.this);
		dialog.setMessage("Saving Image");
		dialog.setCancelable(false);
		dialog.show();
		}
		@Override
		protected String doInBackground(String... params) {
			PhotoData photoData = new PhotoData();
			final String fileName = data.getInterviewName()+"_"+new Date().getTime();
			PutObjectRequest por = new PutObjectRequest( AppConstants.BUCKET_NAME, fileName, new java.io.File( fileLocation) );  
			s3Client.putObject( por );
			ResponseHeaderOverrides override = new ResponseHeaderOverrides();
			// Generate the presigned URL.

			// Added an hour's worth of milliseconds to the current time.
			Date expirationDate = new Date(
			System.currentTimeMillis() + 2100000000);
			GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(
					AppConstants.BUCKET_NAME, fileName);
			urlRequest.setExpiration(expirationDate);
			urlRequest.setResponseHeaders(override);

			URL url = s3Client.generatePresignedUrl(urlRequest);

			String recordingURL = url.toString();
			photoData.project = (mProjectHandler.getCurrentProject());
			photoData.fileURL= (recordingURL);
			photoData.interviewName = data.getInterviewName();
			//
			
			//start the azure now
			//recordingbucket
			try {
				MobileServiceClient  mClient = new MobileServiceClient(
						"https://object.azure-mobile.net/",
						"cvnQzPqZEvwdjPOdpCQYlXoSNbSMMz99", ASessionActivity.this);
				MobileServiceTable<PhotoData> mToDoTable = mClient.getTable("photointerview",PhotoData.class);
				mToDoTable.insert(photoData, new TableOperationCallback<PhotoData>(){

					@Override
					public void onCompleted(PhotoData arg0, Exception arg1,
							ServiceFilterResponse arg2) {
						dialog.dismiss();
						runOnUiThread(new Runnable(){

							@Override
							public void run() {
								Toast.makeText(ASessionActivity.this, "Image saved to the server and to:\n" +
					            		fileName, Toast.LENGTH_LONG).show();
								
							}
							
						});
			            
					}
					
				});
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String result) {

			

			
			}
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_asession);
		data = (InterviewData) getIntent().getSerializableExtra("INTERVIEW_DATA");
		Button button = (Button) findViewById(R.id.addPictureButton);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
				//Take Picture
				// create Intent to take a picture and return control to the calling application
			    
				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
                /*
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

			    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
			    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

			    // start the image capture Intent
			    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);*/

			}
		});
		
		super.setTitle(data.getInterviewName()+" Session");
		super.highlightDrawerItem(2);
		
		mProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);

		// Initialize the progress bar
		mProgressBar.setVisibility(ProgressBar.GONE);

		try {
			// Create the Mobile Service Client instance, using the provided
			// Mobile Service URL and key
			mClient = new MobileServiceClient(
					"https://object.azure-mobile.net/",
					"cvnQzPqZEvwdjPOdpCQYlXoSNbSMMz99", this)
					.withFilter(new ProgressFilter());

			// Get the Mobile Service Table instance to use
			mToDoTable = mClient.getTable(ToDoItem.class);

			mTextNewToDo = (EditText) findViewById(R.id.textNewToDo);

			// Create an adapter to bind the items with the view
			mAdapter = new ToDoItemAdapter(this, R.layout.row_list_to_do);
			ListView listViewToDo = (ListView) findViewById(R.id.listViewToDo);
			listViewToDo.setAdapter(mAdapter);

			// Load the items from the Mobile Service
			refreshItemsFromTable();

		} catch (MalformedURLException e) {
			createAndShowDialog(
					new Exception(
							"There was an error creating the Mobile Service. Verify the URL"),
					"Error");
		}
	}
	private static File getOutputMediaFile(int type){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_PICTURES), "MyCameraApp");
	    // This location works best if you want the created images to be shared
	    // between applications and persist after your app has been uninstalled.

	    // Create the storage directory if it does not exist
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("MyCameraApp", "failed to create directory");
	            return null;
	        }
	    }

	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "IMG_"+ timeStamp + ".jpg");
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "VID_"+ timeStamp + ".mp4");
	    } else {
	        return null;
	    }

	    return mediaFile;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_refresh) {
			refreshItemsFromTable();
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected String[] getNavDrawerListTitles() {
		return AppConstants.NAV_DRAWER_TITLE;
	}

	@Override
	protected void selectedDrawerItem(int position) {
		switch (position) {
		case 0:
			Intent intent3 = new Intent(this, ToDoActivity.class);
			startActivity(intent3);
			break;
		case 1:
			Intent intent = new Intent(this,RecordInterviewActivity.class);
			startActivity(intent);
			break;
		case 2:
			//sessin
			Intent intent12 = new Intent(this,RecordSessionActivity.class);
			startActivity(intent12);
			break;
		case 3:
			new ProjectManager(this).switchProject();
			Intent intent2 = new Intent(this,ProjectSelectionActivity.class);
			startActivity(intent2);
			break;
		default:
			break;
		}

	}

	/**
	 * Mark an item as completed
	 * 
	 * @param item
	 *            The item to mark
	 */
	public void checkItem(ToDoItem item) {
		if (mClient == null) {
			return;
		}

		// Set the item as completed and update it in the table
		item.setComplete(true);

		mToDoTable.update(item, new TableOperationCallback<ToDoItem>() {

			public void onCompleted(ToDoItem entity, Exception exception,
					ServiceFilterResponse response) {
				if (exception == null) {
					if (entity.isComplete()) {
						mAdapter.remove(entity);
					}
				} else {
					createAndShowDialog(exception, "Error");
				}
			}

		});
	}

	/**
	 * Add a new item
	 * 
	 * @param view
	 *            The view that originated the call
	 */
	public void addItem(View view) {
		if (mClient == null) {
			return;
		}

		// Create a new item
		ToDoItem item = new ToDoItem();

		item.wildOb = data.getInterviewName();
		item.setText(mTextNewToDo.getText().toString());
		item.setComplete(false);
		item.setProject(mProjectHandler.getCurrentProject());
		//item.set
		// Insert the new item
		mToDoTable.insert(item, new TableOperationCallback<ToDoItem>() {

			public void onCompleted(ToDoItem entity, Exception exception,
					ServiceFilterResponse response) {

				if (exception == null) {
					if (!entity.isComplete()) {
						mAdapter.add(entity);
					}
				} else {
					createAndShowDialog(exception, "Error");
				}

			}
		});

		mTextNewToDo.setText("");
	}

	/**
	 * Refresh the list with the items in the Mobile Service Table
	 */
	private void refreshItemsFromTable() {

		// Get the items that weren't marked as completed and add them in the
		// adapter
		mToDoTable.where().field("project").eq(mProjectHandler.getCurrentProject()).and().field("wildob").eq(data.getInterviewName())
				.execute(new TableQueryCallback<ToDoItem>() {

					public void onCompleted(List<ToDoItem> result, int count,
							Exception exception, ServiceFilterResponse response) {
						if (exception == null) {
							mAdapter.clear();

							for (ToDoItem item : result) {
								mAdapter.add(item);
							}

						} else {
							createAndShowDialog(exception, "Error");
						}
					}
				});
	}

	/**
	 * Creates a dialog and shows it
	 * 
	 * @param exception
	 *            The exception to show in the dialog
	 * @param title
	 *            The dialog title
	 */
	private void createAndShowDialog(Exception exception, String title) {
		Throwable ex = exception;
		if (exception.getCause() != null) {
			ex = exception.getCause();
		}
		createAndShowDialog(ex.getMessage(), title);
	}

	/**
	 * Creates a dialog and shows it
	 * 
	 * @param message
	 *            The dialog message
	 * @param title
	 *            The dialog title
	 */
	private void createAndShowDialog(String message, String title) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setMessage(message);
		builder.setTitle(title);
		builder.create().show();
	}

	private class ProgressFilter implements ServiceFilter {

		@Override
		public void handleRequest(ServiceFilterRequest request,
				NextServiceFilterCallback nextServiceFilterCallback,
				final ServiceFilterResponseCallback responseCallback) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (mProgressBar != null)
						mProgressBar.setVisibility(ProgressBar.VISIBLE);
				}
			});

			nextServiceFilterCallback.onNext(request,
					new ServiceFilterResponseCallback() {

						@Override
						public void onResponse(ServiceFilterResponse response,
								Exception exception) {
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									if (mProgressBar != null)
										mProgressBar
												.setVisibility(ProgressBar.GONE);
								}
							});

							if (responseCallback != null)
								responseCallback
										.onResponse(response, exception);
						}
					});
		}
	}
}
