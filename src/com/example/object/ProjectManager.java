package com.example.object;

import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;

public class ProjectManager {
	Context context;
	private String defaultName = "VERY LONG DEFAULT SO THIS SHOULD NOT OCCUR;";
	public ProjectManager(Context context){
		this.context = context;
	}
	
	public void storeCurrentProject(String project){
		project = project.toUpperCase(Locale.ENGLISH);
		SharedPreferences settings = getSharedPreferences();
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("Project", project);
		editor.commit();
	}
	
	public void storePersonName(String name){
		SharedPreferences settings = getSharedPreferences();
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("PersonName", name);
		editor.commit();
	}
	
	public String getPersonName(){
		SharedPreferences pref = getSharedPreferences();
		return pref.getString("PersonName", defaultName);
	}
	
	public String getCurrentProject(){
		SharedPreferences pref = getSharedPreferences();
		return pref.getString("Project", defaultName);
	}
	
	public void switchProject(){
		storeCurrentProject(defaultName);
	}
	
	public boolean isProjectNameDefined(){
		if(getCurrentProject().equals(defaultName)){
			return false;
		}
		return true;
	}
	/**
	 * 
	 * @return
	 */
	public SharedPreferences getSharedPreferences() {
		SharedPreferences settings = context.getSharedPreferences("P_Name", 0);
		return settings;

	}
}
