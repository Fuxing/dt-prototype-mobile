package com.example.object;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ProjectSelectionActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_project_selection);
		final EditText projectNameET = (EditText) findViewById(R.id.projectNameET);
		final EditText yourNameET = (EditText) findViewById(R.id.yourNameET);
		Button createOrToGoButton = (Button) findViewById(R.id.createOrToGoButton);
		
		createOrToGoButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String projectName = projectNameET.getText().toString();
				String personName = yourNameET.getText().toString();
				if(TextUtils.isEmpty(projectName) || TextUtils.isEmpty(personName)){
					Toast.makeText(ProjectSelectionActivity.this, "Make sure project name and your name is not empty.", Toast.LENGTH_LONG).show();
				}else{
					ProjectManager handler = new ProjectManager(ProjectSelectionActivity.this);
					handler.storeCurrentProject(projectName);
					handler.storePersonName(personName);
					Intent intent = new Intent(ProjectSelectionActivity.this,ToDoActivity.class);
					startActivity(intent);
				}
				
			}
		});

	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.project_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}



}
