package com.example.object;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.TableOperationCallback;
import com.ravolo.lib.android.nav.NavDrawerActivity;
import com.skd.androidrecording.audio.AudioRecordingHandler;
import com.skd.androidrecording.audio.AudioRecordingThread;
import com.skd.androidrecording.visualizer.VisualizerView;
import com.skd.androidrecording.visualizer.renderer.BarGraphRenderer;

public class RecordInterviewActivity extends NavDrawerActivity {
	private AmazonS3Client s3Client = new AmazonS3Client(
			new BasicAWSCredentials(AppConstants.ACCESS_KEY,
					AppConstants.SECRET_KEY));
	private static String mFileName = null;

	private MediaRecorder mRecorder = null;
	ProjectManager mProjectHandler = new ProjectManager(this);
	private InterviewData currentInterviewData = null;
	private VisualizerView visualizerView;

	private void onRecord(boolean start) { 
		if (start) {
			startRecording();
		} else {
			stopRecording();
		}
	}

	private boolean establishData() {
		currentInterviewData = new InterviewData();
		EditText intername = ((EditText) findViewById(R.id.interNameEditText));
		String interviewName = intername.getText().toString();
		if (TextUtils.isEmpty(interviewName)) {
			return false;
		}
		intername.setEnabled(false);

		currentInterviewData.setInterviewName(interviewName);
		EditText interContact = ((EditText) findViewById(R.id.interContactNumEditText));
		String interviewContact = interContact.getText().toString();
		interContact.setEnabled(false);

		EditText interOcc = ((EditText) findViewById(R.id.interOccupationEditText));
		String interviewOccupation = interOcc.getText().toString();
		interOcc.setEnabled(false);

		EditText interAge = ((EditText) findViewById(R.id.interAgeEditText));
		String interviewAge = interAge.getText().toString();
		interAge.setEnabled(false);

		EditText interNote = ((EditText) findViewById(R.id.interNotesEditText));
		String interviewNotes = interNote.getText().toString();
		interNote.setEnabled(false);

		if (TextUtils.isEmpty(interviewContact)) {
			interviewContact = "";
		}
		currentInterviewData.setContactNumber(interviewContact);

		if (TextUtils.isEmpty(interviewOccupation)) {
			interviewOccupation = "";
		}
		currentInterviewData.setOccupation(interviewOccupation);

		if (TextUtils.isEmpty(interviewAge)) {
			interviewAge = "";
		}
		currentInterviewData.setAge(interviewAge);

		if (TextUtils.isEmpty(interviewNotes)) {
			interviewNotes = "";
		}
		currentInterviewData.setNotes(interviewNotes);

		return true;// ran it course
	}

	String filetype = ".wav";

	private void startRecording() {
		if (!establishData()) {
			// unable to establish dat
			new AlertDialog.Builder(this)
					.setMessage(
							"The person you are interviewing have a name rite?")
					.setPositiveButton("Oh Okay",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();

								}
							}).show();
			return;
		}
		((TextView) findViewById(R.id.recordingIndicator))
				.setText("Recording : (Time...)");
		mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
		mFileName += "/" + currentInterviewData.getAudioFileNameInBucket()
				+ filetype;
		Log.e("EEE", mFileName);
		
		/*
		 * mRecorder = new MediaRecorder();
		 * mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		 * mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		 * mRecorder.setOutputFile(mFileName);
		 * mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		 * 
		 * try { mRecorder.prepare(); } catch (IOException e) { new
		 * AlertDialog.Builder(this) .setMessage("Record IO Error, " +
		 * e.getMessage()) .setTitle("Error") .setPositiveButton("OK", new
		 * DialogInterface.OnClickListener() {
		 * 
		 * @Override public void onClick(DialogInterface dialog, int which) {
		 * dialog.cancel();
		 * 
		 * } }).show(); }
		 */
		// InterviewSession

		MobileServiceClient mClient;
		try {
			mClient = new MobileServiceClient(
					"https://object.azure-mobile.net/",
					"cvnQzPqZEvwdjPOdpCQYlXoSNbSMMz99",
					RecordInterviewActivity.this);
			MobileServiceTable<InterviewData> mToDoTable = mClient.getTable(
					"recordingbucket", InterviewData.class);
			mToDoTable.insert(currentInterviewData,
					new TableOperationCallback<InterviewData>() {
						@Override
						public void onCompleted(InterviewData arg0,
								Exception arg1, ServiceFilterResponse arg2) {
							currentInterviewData = arg0;
							Toast.makeText(getApplication(),
									"Started Recording", Toast.LENGTH_LONG)
									.show();
							startRecordingNew();
						}

					});
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void stopRecording() {
		// can activate by 2 button stop and upload show progress bar and
		// refresh activity
		// Toast.makeText(getApplicationContext(), "Saving Recording",
		// Toast.LENGTH_SHORT).show();
		Toast.makeText(getApplicationContext(), "Saving...", Toast.LENGTH_LONG).show();
		stopRecordingNew();

		// might need to use callback here

	}

	private void uploadData() {
		this.runOnUiThread(new Runnable(){
			@Override
			public void run() {
				new UploadRecordingTask().execute();	
			}
			
		});
	}

	private class UploadRecordingTask extends AsyncTask<String, String, String> {

		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(RecordInterviewActivity.this);
			dialog.setMessage("Saving Recording");
			dialog.setCancelable(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			PutObjectRequest por = new PutObjectRequest(
					AppConstants.BUCKET_NAME,
					currentInterviewData.getAudioFileNameInBucket() + filetype,
					new java.io.File(mFileName));
			s3Client.putObject(por);
			ResponseHeaderOverrides override = new ResponseHeaderOverrides();

			// Generate the presigned URL.

			// Added an hour's worth of milliseconds to the current time.
			Date expirationDate = new Date(
					System.currentTimeMillis() + 2100000000);
			GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(
					AppConstants.BUCKET_NAME,
					currentInterviewData.getAudioFileNameInBucket() + filetype);
			urlRequest.setExpiration(expirationDate);
			urlRequest.setResponseHeaders(override);

			URL url = s3Client.generatePresignedUrl(urlRequest);

			String recordingURL = url.toString();
			currentInterviewData
					.setProject(mProjectHandler.getCurrentProject());
			currentInterviewData.setUrl(recordingURL);
			//

			// start the azure now
			// recordingbucket
			try {
				MobileServiceClient mClient = new MobileServiceClient(
						"https://object.azure-mobile.net/",
						"cvnQzPqZEvwdjPOdpCQYlXoSNbSMMz99",
						RecordInterviewActivity.this);
				MobileServiceTable<InterviewData> mToDoTable = mClient
						.getTable("recordingbucket", InterviewData.class);
				mToDoTable.update(currentInterviewData,
						new TableOperationCallback<InterviewData>() {

							@Override
							public void onCompleted(InterviewData arg0,
									Exception arg1, ServiceFilterResponse arg2) {
								refreshActivity();
								dialog.dismiss();
							}

						});
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String result) {

		}

	}

	private void refreshActivity() {
		Toast.makeText(getApplicationContext(), "Added Interview",
				Toast.LENGTH_SHORT).show();
		Intent intent = new Intent(this, RecordInterviewActivity.class);
		startActivity(intent);
	}

	boolean mStartRecording = true;

	private void setupVisualizer() {
		Paint paint = new Paint();
		paint.setStrokeWidth(5f); // set bar width
		paint.setAntiAlias(true);
		paint.setColor(Color.argb(200, 227, 69, 53)); // set bar color
		BarGraphRenderer barGraphRendererBottom = new BarGraphRenderer(2,
				paint, false);
		visualizerView.addRenderer(barGraphRendererBottom);
	}

	private void stopRecordingNew() {
		if (recordingThread != null) {
			recordingThread.stopRecording();
			recordingThread = null;
		}
	}

	AudioRecordingThread recordingThread;

	private void startRecordingNew() {
		recordingThread = new AudioRecordingThread(mFileName,
				new AudioRecordingHandler() { // pass file name where to store
												// the recorded audio
					@Override
					public void onFftDataCapture(final byte[] bytes) {
						runOnUiThread(new Runnable() {
							public void run() {
								if (visualizerView != null) {
									visualizerView.updateVisualizerFFT(bytes); // update
																				// VisualizerView
																				// with
																				// new
																				// audio
																				// portion
								}
							}
						});
					}

					@Override
					public void onRecordSuccess() {
						uploadData();
					}

					@Override
					public void onRecordingError() {
					}

					@Override
					public void onRecordSaveError() {
					}
				});
		recordingThread.start();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record_interview);
		visualizerView = (VisualizerView) findViewById(R.id.visualizerView);
		setupVisualizer();
		super.setTitle("Interview");
		super.highlightDrawerItem(1);
		s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
		final Button recordButton = (Button) findViewById(R.id.recordButton);
		recordButton.setText("Start recording");
		OnClickListener clicker = new OnClickListener() {
			public void onClick(View v) {
				if (currentInterviewData != null) {
					return;
				}
				onRecord(mStartRecording);
				if (mStartRecording) {
					recordButton.setText("Recording...");
					recordButton.setEnabled(false);
				} else {
					recordButton.setText("Start recording");
				}

				mStartRecording = !mStartRecording;
			}
		};
		recordButton.setOnClickListener(clicker);
		final Button saveButton = (Button) findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (currentInterviewData != null) {
					// check to save or not
					if (mStartRecording) {
						// already stopped
						// do nothing because it is already saved
						Toast.makeText(getApplicationContext(), "Saved",
								Toast.LENGTH_LONG).show();
					} else {
						// haven stop.
						// stop it
						onRecord(false);
					}
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.record_interview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mRecorder != null) {
			mRecorder.release();
			mRecorder = null;
		}
	}

	@Override
	protected String[] getNavDrawerListTitles() {
		return AppConstants.NAV_DRAWER_TITLE;
	}

	@Override
	protected void selectedDrawerItem(int position) {
		switch (position) {
		case 0:
			Intent intent = new Intent(this, ToDoActivity.class);
			startActivity(intent);
			break;
		case 1:
			break;
		case 2:
			// sessin
			Intent intent12 = new Intent(this, RecordSessionActivity.class);
			startActivity(intent12);
			break;
		case 3:
			new ProjectManager(this).switchProject();
			Intent intent2 = new Intent(this, ProjectSelectionActivity.class);
			startActivity(intent2);
			break;
		default:
			break;
		}

	}

	private class S3TaskResult {
		String errorMessage = null;
		Uri uri = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public Uri getUri() {
			return uri;
		}

		public void setUri(Uri uri) {
			this.uri = uri;
		}
	}
}
